import vehicles from "../models/vehicles/vehicles"

export default class EntryService{
    EntryService(customersService, vehiclesService, slotsService, customerVehiclesService){
        this.customers = customersService
        this.vehicles = vehiclesService
        this.slots = slotsService
        this.customerVehicles = customerVehiclesService
    }
    //first check if slot is empty
    //check if customer exists using this.customers
    //if customer exists then check for vehicle
    //if vehicle exists then update customerVehicles table and vehicles table

    async customerEntry(mobileNumber, vehicleNumber){
        let checkCustomerExistance = await customers.checkExistence(mobileNumber)
        if(checkCustomerExistance){
            let checkVehicleExistance = await vehicles.checkExistence(vehicleNumber)
            if(checkVehicleExistance){
                
            }
            else{
                return {
                    message : 'Vehicle does not exist',
                    code : 400
                }
            }
        }
        else{
            return {
                message : 'Customer does not exist',
                code : 400
            }
        }
    }
}
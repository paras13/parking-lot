import vehicles from './vehicles'
import customers from './vehicles'

export default class VehiclesService{
    VehiclesService(){

    }

    checkExistence(registerationNumber){
        let vehicle = await vehicles.findOne({
            where : {
                registerationNumber : registerationNumber
            }
        })

        return vehicle ? true : false
    }

    addVehicle(data){
        // data = {
        //     registerationNumber : "MP04SR2103",
        //     color : "red",
        //     type : "4wheeler"
        // }
        let vehicle = await vehicles.create(data)

        if(vehicle){
            return {
                status : true,
                message : "Vehicle added successfully",
                data : vehicle.id
            }
        }
    }

    getVehicle(registerationNumber){
        let vehicle = await vehicles.findOne({
            where : {
                registerationNumber : registerationNumber
            }
        })

        return {
            status : true,
            message : "Vehicle found successfully",
            data : vehicle
        };
    }
}
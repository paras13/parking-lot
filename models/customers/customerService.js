import customers from './customers'

export default class CustomerService{
    CustomerService(){

    }

    checkExistence(mobileNumber){
        let customer = await customers.findOne({
            where : {
                mobile : mobileNumber
            }
        })

        return customer ? true : false
    }

    createCustomer(data){
        // data = {
        //     name : 'paras',
        //     mobile : 9097985
        // }
        customerId = data.name.substring(0,3) + (data.mobile.toString().slice(5))
        data = {...data, customerId : customerId}
        let customer = await customers.create(data)

        if(customer){
            return {
                status : true,
                message : "Customer added successfully",
                data : customer.id
            }
        }
    }

    getCustomer(mobileNumber){
        let customer = await customers.findOne({
            where : {
                mobile : mobileNumber
            }
        })

        return {
            status : true,
            message : "Customer found successfully",
            data : customer
        };
    }
}
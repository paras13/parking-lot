'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Slots extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Slots.init({
    floor: DataTypes.INTEGER,
    section: DataTypes.CHAR,
    code: DataTypes.INTEGER,
    isOccupied: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Slots',
  });
  return Slots;
};